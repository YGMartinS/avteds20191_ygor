function reqServlet(json, url, callback) {

	var xhttp = new XMLHttpRequest();

	xhttp.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			callback(this.responseText);
		}
	};

	xhttp.open("POST", url, true);
	xhttp.setRequestHeader('Content-type', 'application/json; charset=utf-8');

	if (json != null) {
		xhttp.send(JSON.stringify(json));
	} else {
		xhttp.send();
	}

}

var Cliente = {
	addCliente : function(e) {
		e.preventDefault()

		var t = {}
		var endereco = {}
		t.nome = document.getElementById("nome").value
		t.cpf = document.getElementById("cpf").value
		endereco.logradouro = document.getElementById("logradouro").value
		endereco.numero = document.getElementById("numero").value
		endereco.cidade = document.getElementById("cidade").value
		endereco.estado = document.getElementById("estado").value
		endereco.cep = document.getElementById("cep").value
		t.endereco = endereco

		reqServlet(t, "addcliente", Cliente.callbackAddCliente)

		return false
	},

	callbackAddCliente : function(responseText) {
		var resp = JSON.parse(responseText)
		var msg = document.getElementById("msg")
		msg.style.display = "block"
		msg.innerHTML = resp.msg
	},
	
	retornaCliente : function() {		

		reqServlet(null, "loadcliente", Cliente.callbackRetornaCliente);
	},

	callbackRetornaCliente : function(responseText) {
		var resp = JSON.parse(responseText);
		var tbody = document.querySelector("tbody")
		tbody.innerHTML = "";
		for(let i=0; i<resp.data.length; i++){
			let tr = document.createElement("tr")
			let td = document.createElement("td")

			let nome = td
			let cpf = td
			let logradouro = td
			let numero = td
			let cidade = td
			let estado = td
			let cep = td

			nome.textContent = resp.data[i].nome
			cpf.textContent = resp.data[i].cpf			
			logradouro.textContent = resp.data[i].endereco.logradouro		
			numero.textContent = resp.data[i].endereco.numero			
			cidade.textContent = resp.data[i].endereco.cidade			
			estado.textContent = resp.data[i].endereco.estado			
			cep.textContent = resp.data[i].endereco.cep
			
			tr.appendChild(nome)
			tr.appendChild(cpf)
			tr.appendChild(logradouro)
			tr.appendChild(numero)
			tr.appendChild(cidade)
			tr.appendChild(estado)
			tr.appendChild(cep)
			tbody.appendChild(tr)
		}
		var visTable = document.querySelector("table")
		visTable.style.display = "inline"

	}
	
}
